import Vue from 'vue'
import Router from 'vue-router'

import Layout from '@/components/Layout'


Vue.use(Router)

export default new Router({
    routes: [
      {
        path: '/',
        component: Layout,
        children:[
          {
            path:'/',
            component: () => import('@/views/Home'),
            name:'Home'
          },
          {
            path:'/shop',
            component: () => import('@/views/Shop'),
            name:'Shop'
          },
          {
            path:'/services',
            component: () => import('@/views/Services'),
            name:'Services'
          },
          {
            path:'/services/:id',
            component: () => import('@/views/Reviews'),
            name:'Reviews',
            props: true
          },
          {
            path:'/blog',
            component: () => import('@/views/Blog'),
            name:'Blog'
          },
          {
            path:'/blog/:id',
            component: () => import('@/views/Post'),
            name:'Post',
            props: true

          },
          {
            path:'/login',
            component: () => import('@/views/Login'),
            name:'Login'
          },
          {
            path:'/registration',
            component: () => import('@/views/registration/Registration'),
            name:'Registration'
          },
          {
            path:'/check_mail',
            component: () => import('@/views/registration/RegistrationCheckMail'),
            name:'CheckMail'
          },
          {
            path:'/activate/',
            component: () => import('@/views/registration/RegistrationConfirm'),
            name:'Confirm',
          },
          {
            path:'/change_password/',
            component: () => import('@/views/registration/ChangePassword'),
            name:'ChangePassword',
          },
          {
            path:'/cart',
            component: () => import('@/views/Cart'),
            name:'Cart'
          },
          {
            path:'/success',
            component: () => import('@/views/successCheckoutPage'),
            name:'successCheckoutPage'
          },
        ]
      }
    ],
    mode:'history'
  },

)
