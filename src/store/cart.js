import axios from 'axios'
import store from '@/store'

export default {
  state: {
    cart: {
      items: []
    },
  },
  mutations: {
    initializeStore(state) {
      if (localStorage.getItem('cart')) {
        state.cart = JSON.parse(localStorage.getItem('cart'))
      } else {
        localStorage.setItem('cart', JSON.stringify(state.cart))
      }
    },
    addToCart(state, item) {
      const exists = state.cart.items.filter(i => i.id === item.id)
    if (exists.length) {
      console.log('Этот товар уже в корзине')
    } else {
      state.cart.items.push(item)
    }
    localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    removeFromCart(state, item) {
      state.cart.items = state.cart.items.filter(i => i.id !== item.id)
      localStorage.setItem('cart', JSON.stringify(state.cart))
    },
    clearCart(state) {
      state.cart.items = []
      localStorage.setItem('cart', JSON.stringify(state.cart))
    },
  },
  actions: {
  },
  getters: {
  }
}
