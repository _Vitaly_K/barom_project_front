import axios from 'axios'
import store from '@/store'

export default {
  state: {
    status: '',
    token: localStorage.getItem('token') || '',
    username: localStorage.getItem('username') || ''
  },
  mutations: {
    auth_request(state){
      state.status = 'loading'
    },
    auth_success(state, token, username){
      state.status = 'success'
      state.token = token
      state.username = username
    },
    auth_error(state){
      state.status = 'error'
    },
    logout(state){
      state.status = ''
      state.token = ''
      state.username = ''
    },
  },
  actions: {
    login({commit}, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        axios({
          url: `${store.getters.getServerUrl}api/auth/jwt/create`,
          data: user,
          method: 'POST'
        })
          .then(resp => {
            const token = resp.data.access
            const username = user.username
          localStorage.setItem('token', token)
          localStorage.setItem('username', username)
          commit('auth_success', {token, username})
          resolve(resp)
        })
        .catch(err => {
          commit('auth_error')
          localStorage.removeItem('token')
          localStorage.removeItem('username')
          reject(err)
        })
      })
    },
    register({commit}, user) {
      return new Promise((resolve, reject) => {
        this.$http({
          url: `${store.getters.getServerUrl}api/auth/users/`,
          data: user,
          method: 'POST' })
        .then(resp => {
          console.log('Было отправлено письмо')
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
      })
    },
    confirm({commit}, user) {
      return new Promise((resolve, reject) => {
        this.$http({
          url: `${store.getters.getServerUrl}api/auth/users/activation/`,
          data: user,
          method: 'POST'
        })
          .then(resp => {
            console.log('Активация прошла')
            resolve(resp)
          })
          .catch(err => {
          console.log('Активация не прошла')
          reject(err)
        })
      })
    },
    logout({commit}){
      return new Promise((resolve, reject) => {
        commit('logout')
        localStorage.removeItem('token')
        localStorage.removeItem('username')
        resolve()
      })
    },
    changePassword({commit}, user) {
      return new Promise((resolve, reject) => {
        this.$http({
          url: `${store.getters.getServerUrl}api/auth/users/reset_password/`,
          data: user,
          method: 'POST'
        })
        .then(resp => {
          console.log('Было отправлено письмо')
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
      })
    },
  },

  getters : {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    getUsername: state => state.username,
    getToken: state => state.token
  }
}
