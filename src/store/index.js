import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import cart from "./cart";
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    backendUrl: "http://188.225.79.64/",
    // backendUrl: "http://127.0.0.1:8000/",
    isLoading: false
  },
  mutations: {
    setLoading: (state, status) => {
      state.isLoading = status
    }
  },
  actions: {},
  getters: {
    getServerUrl: state => {
      return state.backendUrl
    }
  },
  modules: {
    auth,
    cart
  }
})

export default store
